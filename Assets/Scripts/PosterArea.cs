
using UnityEngine;

public class PosterArea : MonoBehaviour
{
    private bool occupied;
    [SerializeField] private float timeBonus;
    private bool canPlace;
    private ParticleSystem particleSystem;
    private CameraMovement cameraMovement;
    private SpriteRenderer spriteRend;

    private void Awake()
    {
        particleSystem = GetComponent<ParticleSystem>();
        cameraMovement = FindObjectOfType<CameraMovement>();
        spriteRend = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        if (transform.parent != null)
        {
            spriteRend.sortingOrder = transform.parent.GetComponent<SpriteRenderer>().sortingOrder + 1;
        }
    }

    // The centre position of this object is the place where the poster is put.
    // The OnTriggerEnter is the radius from which you can place the poster.
    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<Player>();
        if (!player)
        {
            return;
        }
        cameraMovement.ZoomIn();
        canPlace = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var player = other.GetComponent<Player>();
        if (!player)
        {
            return;
        }
        
        cameraMovement.ZoomDefault();
        canPlace = false;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Place") && canPlace)
        {
            PlacePoster();
        }
    }

    private void PlacePoster()
    {
        if (occupied)
        {
            // Negative sfx
            return;
        }

        var player = FindObjectOfType<Player>();

        if (player.transform.position.y > transform.position.y)  // Player cannot place from above.
        {
            return;
        }
        
        if (player.GetPosters() <= 0)
        {
            // Negative sfx
            player.EmitNoPosters();
            return;
        }
        player.PlacePoster();
        InstantiatePoster();
        occupied = true;
        player.GiveTime(timeBonus);
        particleSystem.Stop();
    }

    private void InstantiatePoster()
    {
        var posterPrefab = Resources.Load<Poster>("prefabs/Poster");
        var poster = Instantiate(posterPrefab);
        poster.SetTexture(Game.Meme.Texture);
        poster.transform.position = transform.position;
        poster.ParentRenderer = GetComponent<SpriteRenderer>();
        poster.UpdateSortingLayer();
    }
}
