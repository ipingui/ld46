
using System.Collections;
using TMPro;
using UnityEngine;

public class FadingScore : MonoBehaviour
{
    private const float FadeOutSpeed = 0.1f;
    private const float MovementSpeed = 0.01f;

    private TextMeshProUGUI text;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
        StartCoroutine(FadeOut());
    }

    private void Update()
    {
        transform.Translate(new Vector3(0, MovementSpeed, 0));
    }

    IEnumerator FadeOut()
    {
        while (text.color.a > 0.001f)
        {
            var alpha = Mathf.SmoothStep(text.color.a, 0, FadeOutSpeed);
            text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
            yield return null;
        }
        Destroy(gameObject);
        yield return null;
    }
}
