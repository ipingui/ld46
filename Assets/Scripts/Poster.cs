
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poster : MonoBehaviour
{
    private SpriteRenderer spriteRend;
    private bool done;
    private AudioSource sfx;
    private float tweenDelay;
    public SpriteRenderer ParentRenderer { get; set; }

    private Color GetRandomColor()
    {
        return Random.ColorHSV(0f, 1f, 0f, 0.5f, 0.5f, 1f);
    }

    private void Awake()
    {
        spriteRend = GetComponent<SpriteRenderer>();
        transform.localScale = 12*Vector3.one;
        var c = spriteRend.color;
        spriteRend.color = GetRandomColor();
        sfx = GetComponent<AudioSource>();
        tweenDelay = GetComponent<TweenTransforms>().duration;
        StartCoroutine(Sfx());
    }

    private void Update()
    {
        if (done)
        {
            return;
        }

        done = true;
        var c = spriteRend.color;
        spriteRend.color = new Color(c.r, c.g, c.b, 1);
        
    }

    public void SetTexture(Texture2D tex)
    {
        var newSprite = Sprite.Create(tex, new Rect(Vector2.zero, new Vector2(tex.width, tex.height)), new Vector2(0.5f, 0.5f));
        spriteRend.sprite = newSprite;
        transform.Rotate(0, 0, 180);
        transform.localScale = 1.5f * Vector3.one;
    }

    public void UpdateSortingLayer()
    {
        spriteRend.sortingLayerName = "ObjectsFront";
        spriteRend.sortingOrder = ParentRenderer.sortingOrder;
    }

    IEnumerator Sfx()
    {
        var clip = Resources.Load<AudioClip>("sfx/poster_start");
        sfx.PlayOneShot(clip);
        yield return new WaitForSeconds(tweenDelay);
        sfx.Stop();
        clip = Resources.Load<AudioClip>("sfx/poster_end");
        sfx.PlayOneShot(clip);
        
    }
}
