
using System.Collections.Generic;
using UnityEngine;

public static class Game
{
    public static Meme Meme { get; set; }
    private static List<Name> PeopleWhoHaveSeenMeme = new List<Name>();
    
    public static bool HasStarted { get; set; }
    public static bool IsPaused { get; set; }
    public static bool Over { get; set; }

    public static void UpdatePeopleList(Name person)
    {
        if (PeopleWhoHaveSeenMeme.Contains(person))
        {
            return;
        }
        
        PeopleWhoHaveSeenMeme.Add(person);
    }

    public static void Reset()
    {
        PeopleWhoHaveSeenMeme = new List<Name>();
        HasStarted = false;
        Time.timeScale = 1;
        IsPaused = false;
        Over = false;
    }
}
