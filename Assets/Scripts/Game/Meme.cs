
using UnityEngine;

public class Meme
{
    public string Name { get; set; }
    public Texture2D Texture { get; set; }

    public Meme(string name)
    {
        Name = name;
    }
}
