
using System.Collections;
using UnityEngine;

public class WinLoseManager : MonoBehaviour
{
    private AudioSource sfx;
    private Rect rect;

    private void Awake()
    {
        rect = GetComponent<RectTransform>().rect;
        sfx = GetComponent<AudioSource>();
    }

    public void WinGame()
    {
        var prefab = Resources.Load<GameObject>("prefabs/win");
        var win = Instantiate(prefab);
        win.transform.SetParent(transform);
        win.transform.localScale = Vector3.one;
        win.transform.localPosition = new Vector3(0, 0);
        StartCoroutine(Win());
    }

    IEnumerator Win()
    {
        var clip = Resources.Load<AudioClip>("sfx/build_up");
        sfx.PlayOneShot(clip);
        yield return new WaitForSeconds(2f);
        clip = Resources.Load<AudioClip>("sfx/success");
        sfx.PlayOneShot(clip);
    }
    
    public void LoseGame()
    {
        var prefab = Resources.Load<GameObject>("prefabs/lose");
        var lose = Instantiate(prefab);
        lose.transform.SetParent(transform);
        lose.transform.localScale = Vector3.one;
        lose.transform.localPosition = new Vector3(0, 0);
        var clip = Resources.Load<AudioClip>("sfx/lose");
        sfx.PlayOneShot(clip);
    }

}
