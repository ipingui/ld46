
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    private float timeRemaining;
    private int posterCount;
    private const float MaxTime = 30f;  // In seconds.
    private Rigidbody2D rigidbody2D;
    private AudioSource sfx;
    
    private PlayerMovement playerMovement;
    private UIHandler uiHandler;
    private WinLoseManager winLoseManager;

    private Coroutine footstepCoroutine;
    
    public int PeopleTalkedTo { get; set; }
    public int PostersPlaced { get; private set; }

    private int maxPosters;
    private int maxPeople;

    [SerializeField] private bool onMenu;

    private void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        playerMovement = GetComponent<PlayerMovement>();
        uiHandler = FindObjectOfType<UIHandler>();
        sfx = GetComponent<AudioSource>();
        winLoseManager = FindObjectOfType<WinLoseManager>();
        if (onMenu)
        {
            return;
        }

        maxPeople = FindObjectsOfType<Npc>().Length;
        maxPosters = FindObjectsOfType<PosterArea>().Length;
        ResetPlayer();
    }

    private void Update()
    {
        if (!Game.HasStarted)
        {
            return;
        }
        HandleTime();
        HandleSfx();
        CheckForWin();
        rigidbody2D.velocity = Vector2.zero;
    }

    private void HandleTime()
    {
        if (!playerMovement.CanMove)
        {
            return;
        }
        timeRemaining -= Time.deltaTime;  // How much time has passed since the last frame.
        if (timeRemaining < 0)
        {
            timeRemaining = 0;
            LoseGame();
        }
        uiHandler.UpdateHealthBar(timeRemaining/MaxTime);
    }

    private void HandleSfx()
    {
        if (playerMovement.Running && footstepCoroutine == null)
        {
            footstepCoroutine = StartCoroutine(PlayFootsteps());
        }
        else if (!playerMovement.Running && footstepCoroutine != null)
        {
            StopCoroutine(footstepCoroutine);
            footstepCoroutine = null;
        }
    }

    public void PlacePoster()
    {
        PostersPlaced += 1;
        GivePosters(-1);
    }

    public int GetPosters()
    {
        return posterCount;
    }

    public void ResetPlayer()
    {
        timeRemaining = MaxTime;
        posterCount = 0;
        PostersPlaced = 0;
        PeopleTalkedTo = 0;
        GivePosters(3);
    }

    public void GivePosters(int amount)
    {
        posterCount += amount;
        if (posterCount < 0)
        {
            posterCount = 0;
        }
        uiHandler.UpdatePosterNumber(posterCount); 
    }

    public void GiveTime(float amount)
    {
        timeRemaining += amount;
        if (timeRemaining > MaxTime)
        {
            timeRemaining = MaxTime;
        }
        EmitNice();
    }

    public void EmitNoPosters()
    {
        var prefab = Resources.Load<GameObject>("prefabs/NoPosters!");
        var noPosters = Instantiate(prefab);
        noPosters.transform.position = transform.position;
        noPosters.transform.SetParent(transform);
    }

    private void CheckForWin()
    {
        if (PostersPlaced < maxPosters || PeopleTalkedTo < maxPeople || Game.Over)
        {
            return;
        }
        
        WinGame();
    }
    
    public void EmitNice()
    {
        var prefab = Resources.Load<GameObject>("prefabs/Nice!");
        var nice = Instantiate(prefab);
        nice.transform.position = transform.position;
        nice.transform.SetParent(transform);
    }

    private void WinGame()
    {
        Game.Over = true;
        playerMovement.CanMove = false;
        winLoseManager.WinGame();
    }

    private void LoseGame()
    {
        Game.Over = true;
        playerMovement.CanMove = false;
        winLoseManager.LoseGame();
    }

    IEnumerator PlayFootsteps()
    {
        const float interval = 0.5f;
        var clip = Resources.Load<AudioClip>("sfx/step");
        while (playerMovement.Running)
        {
            sfx.PlayOneShot(clip);
            yield return new WaitForSeconds(interval);
        }
        yield return null;
    }
}
