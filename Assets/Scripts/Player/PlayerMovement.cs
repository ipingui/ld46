
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float previousHorizontalInput;
    private float previousVerticalInput;
    [SerializeField] private float movementSpeed;
    private const float SpeedModifier = 0.05f;
    private Animator animator;
    private SpriteRenderer spriteRend;
    public bool Running { get; private set; }
    
    public bool CanMove { get; set; }

    private void Awake()
    {
        CanMove = true;
        animator = GetComponent<Animator>();
        spriteRend = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (!Game.HasStarted)
        {
            return;
        }
        Move();
    }

    private void Move()
    {
        if (!CanMove)
        {
            return;
        }
        var movementVector = Vector3.zero;
        movementVector.x = Input.GetAxisRaw("Horizontal");
        movementVector.y = Input.GetAxisRaw("Vertical");
        movementVector.Normalize();
        Running = movementVector.magnitude != 0;
        animator.SetBool("Running", Running);
        if (movementVector.x > 0)
        {
            spriteRend.flipX = true;
        }

        if (movementVector.x < 0)
        {
            spriteRend.flipX = false;
        }
        
        transform.position = transform.position + movementSpeed*SpeedModifier*movementVector;
    }

}
