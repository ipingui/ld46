
using System;
using System.Collections;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private Player player;
    private const float MovementSpeed = 0.2f;
    private float originalSize;

    private Camera cam;

    private enum Mode
    {
        Draw,
        Game
    }

    private Mode currentMode;
    private Transform drawingCanvas;
    private Coroutine zoomCoroutine;

    private void Awake()
    {
        player = FindObjectOfType<Player>();
        cam = GetComponent<Camera>();
        originalSize = cam.orthographicSize;
        drawingCanvas = FindObjectOfType<MemeDrawingCanvas>().transform;
        DrawMode();
    }

    private void Start()
    {
    }

    public void GameMode()
    {
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);
        currentMode = Mode.Game;
        cam.orthographicSize = originalSize;
        Debug.Log("Switched to Game Mode.");
    }

    public void DrawMode()
    {
        currentMode = Mode.Draw;
        cam.orthographicSize = 3f;
        Debug.Log("Switched to Draw Mode.");
    }

    private void Update()
    {
        FollowPlayer();
        ShowDrawingCanvas();
    }

    private void ShowDrawingCanvas()
    {
        if (currentMode != Mode.Draw)
        {
            return;
        }

        transform.position = new Vector3(drawingCanvas.position.x, drawingCanvas.position.y, transform.position.z);
    }

    private void FollowPlayer()
    {
        if (currentMode != Mode.Game)
        {
            return;
        }
        var playerLerpVector = Vector2.Lerp(transform.position, player.transform.position, MovementSpeed);
        var playerLerpVector2 = Vector2.Lerp(transform.position, playerLerpVector, MovementSpeed);
        transform.position = new Vector3(playerLerpVector2.x, playerLerpVector2.y, transform.position.z);
    }

    public void ZoomIn()
    {
        PerformZoom(1.5f);
    }

    public void ZoomDefault()
    {
        PerformZoom(originalSize);
    }

    private void PerformZoom(float zoomValue)
    {
        if (zoomCoroutine != null)
        {
            StopCoroutine(zoomCoroutine);
        }
        
        zoomCoroutine = StartCoroutine(Zoom(zoomValue));
    }

    IEnumerator Zoom(float zoomValue, float speed=0.2f)
    {
        while (Math.Abs(cam.orthographicSize - zoomValue) > 0.01)
        {
            cam.orthographicSize = Mathf.SmoothStep(cam.orthographicSize, zoomValue, speed);
            yield return null;
        }

        cam.orthographicSize = zoomValue;
        yield return null;
    }
}
