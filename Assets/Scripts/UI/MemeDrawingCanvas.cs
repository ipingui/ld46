
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class MemeDrawingCanvas : MonoBehaviour
{
    private Camera cam;
    private const int MemeXDimension = 24;
    private const int MemeYDimension = 36;
    private Texture2D currentTexture;
    [SerializeField] private Text memeName;
    [SerializeField] private GameObject drawingOverlayContents;

    private void Awake()
    {
        cam = FindObjectOfType<Camera>();
    }

    private void Start()
    {
        InitialiseTexture();
    }

    private void Update()
    {
        if (Game.HasStarted)
        {
            return;
        }
        HandleDrawing();
        HandleInput();
    }

    private void HandleInput()
    {
        if (Input.GetButtonDown("Finish"))
        {
            Finish();
        }
    }

    private void InitialiseTexture()
    {
        // Initialise with an empty texture.
        var texture = new Texture2D(MemeXDimension, MemeYDimension);
        var texArray = texture.GetPixels();
        for (int i = 0; i < texArray.Length; i++)
        {
            texArray[i] = Color.white;
        }
        texture.SetPixels(texArray);
        texture.filterMode = FilterMode.Point;
        var rend = GetComponent<Renderer>();
        rend.material.mainTexture = texture;
        texture.Apply();
        currentTexture = texture;
    }

    public void Clear()
    {
        InitialiseTexture();
    }

    private void HandleDrawing()
    {
        if (!Input.GetButton("Draw"))
        {
            return;
        }
        RaycastHit hit;

        if (!Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit))
        {
            return;
        }

        var rend = hit.transform.GetComponent<Renderer>();
        var meshCollider = hit.collider as MeshCollider;

        if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
            return;

        var tex = rend.material.mainTexture as Texture2D;
        var pixelUV = hit.textureCoord;
        pixelUV.x *= tex.width;
        pixelUV.y *= tex.height;

        tex.SetPixel((int)pixelUV.x, (int)pixelUV.y, Color.black);
        tex.Apply();
        currentTexture = tex;
    }

    public void Finish()
    {
        cam.GetComponent<CameraMovement>().GameMode();
        if (memeName.text == "")
        {
            Game.Meme = new Meme("Ugandan Knuckles");
        }
        else
        {
            Game.Meme = new Meme(memeName.text);
        }
        Game.Meme.Texture = currentTexture;
        Game.HasStarted = true;
        FindObjectOfType<UIHandler>().Activate();
        drawingOverlayContents.SetActive(false);
    }
}
