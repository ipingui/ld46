
using UnityEngine;

public class RestartButton : Button
{
    // This is a quick fix hack.
    private PauseMenu pauseMenu;

    protected override void Awake()
    {
        base.Awake();
        pauseMenu = FindObjectOfType<PauseMenu>();
    }

    protected override void Update()
    {
        if (Selected && Input.GetButtonDown("Draw"))
        {
            Selected = false;
            pauseMenu.Restart();
        }
    }
}
