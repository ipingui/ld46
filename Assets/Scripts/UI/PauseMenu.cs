
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject background;
    [SerializeField] private Button button;

    private void Awake()
    {
        background.SetActive(false);
    }

    private void Update()
    {
        if (!Game.HasStarted || Game.Over)
        {
            return;
        }
        
        if (Input.GetButtonDown("Cancel"))
        {
            if (Game.IsPaused)
            {
                UnPause();
                return;
            }
            
            Pause();
        }
    }

    private void Pause()
    {
        Game.IsPaused = true;
        Time.timeScale = 0;
        background.SetActive(true);
    }

    public void UnPause()
    {
        Game.IsPaused = false;
        Time.timeScale = 1;
        background.SetActive(false);
        button.Deselect();
    }

    public void Restart()
    {
        Game.Reset();
        SceneManager.LoadScene(1);
    }
}
