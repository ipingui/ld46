
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SpeechBubble : MonoBehaviour
{
    // Attach this to an instantiated speech bubble when a dialogue is triggered.
    public Queue<string> Dialogue { private get; set; }  // This is set upon instantiation.
    public Npc Owner { private get; set; }

    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI sentenceText;
    [SerializeField] private TweenTransforms openingTween;
    [SerializeField] private TweenTransforms closingTween;

    private AudioSource sfx;

    private const string MemeSyntax = "%MEME%";  // When reading the meme out.

    private void Awake()
    {
        transform.localScale = Vector3.zero;
        sfx = GetComponent<AudioSource>();
    }

    private void Start()
    {
        nameText.text = Owner.GetName().ToString();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Talk") && !Tweening())
        {
            AdvanceDialogue();
        }
    }

    public bool Tweening()
    {
        return openingTween.isPlaying || closingTween.isPlaying;
    }

    public void AdvanceDialogue()
    {
        if (Dialogue.Count == 0)
        {
            EndDialogue();
            return;
        }
        
        var sentence = Dialogue.Dequeue();
        DisplaySentence(sentence);
        sfx.PlayOneShot(sfx.clip);
    }

    private void EndDialogue()
    {
        var playerMovement = FindObjectOfType<PlayerMovement>();
        playerMovement.CanMove = true;
        Owner.InConversation = false;
        Owner.ShowMeme();
        var delay = closingTween.duration;
        closingTween.Begin();
        Destroy(transform.parent.gameObject, delay);
    }

    public void SetPosition(Vector3 speakerPosition)
    {
        transform.position = speakerPosition;
    }

    private void DisplaySentence(string sentence)
    {
        sentence = sentence.Replace(MemeSyntax, Game.Meme.Name);
        sentenceText.text = sentence;
    }
}
