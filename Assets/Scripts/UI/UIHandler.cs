
using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class UIHandler : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI memeHealth;
    [SerializeField] private TextMeshProUGUI posterNumber;
    [SerializeField] private GameObject contents;
    [SerializeField] private TextMeshProUGUI placedPosterNumber;
    [SerializeField] private TextMeshProUGUI peopleTalkedToNumber;

    private int posterCount;
    private int personCount;

    private Player player;
    
    private void Awake()
    {
        player = FindObjectOfType<Player>();
    }

    private void Start()
    {
        contents.SetActive(false);
        personCount = FindObjectsOfType<Npc>().Length;
        posterCount = FindObjectsOfType<PosterArea>().Length;
    }

    private void Update()
    {
        placedPosterNumber.text = player.PostersPlaced + " / " + posterCount;
        peopleTalkedToNumber.text = player.PeopleTalkedTo + " / " + personCount;
    }

    public void Activate()
    {
        contents.SetActive(true);
    }
    
    public void Deactivate()
    {
        contents.SetActive(false);
    }

    public void UpdateHealthBar(float percentage)
    {
        memeHealth.text = Decimal.Round((decimal)percentage*100, 2) + "%";
    }

    public void UpdatePosterNumber(int number)
    {
        posterNumber.text = number.ToString();
    }
}
