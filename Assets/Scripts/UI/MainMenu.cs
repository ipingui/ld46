
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private TweenAlpha fadeOverlay;
    public void StartGame()
    {
        fadeOverlay.Begin();
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(fadeOverlay.duration);
        SceneManager.LoadScene(1);
    }
}
