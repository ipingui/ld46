
using System.Collections.Generic;
using UnityEngine;

public class Npc : EnvironmentObject
{
    [SerializeField] private List<string> initialDialogue;
    [SerializeField] private List<string> doneDialogue;
    [SerializeField] private Name npcName;
    [SerializeField] private float timeBonus;
    private bool canTalk;
    public bool InConversation { get; set; }
    private bool hasSeenMeme;

    private SpeechBubble currentSpeechBubble;

    protected override void Awake()
    {
        base.Awake();
    }

    // They should have a trigger collider than specifies the range at which the player can talk to them.
    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<Player>();
        if (!player)
        {
            return;
        }

        canTalk = true;
        // Make icon appear above head.
    }

    public Name GetName()
    {
        return npcName;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var player = other.GetComponent<Player>();
        if (!player)
        {
            return;
        }

        canTalk = false;
        // Make icon disappear above head.
    }

    protected override void Update()
    {
        base.Update();
        if (canTalk && Input.GetButtonDown("Talk") && !InConversation)
        {
            if (currentSpeechBubble != null && currentSpeechBubble.Tweening())
            {
                return;
            }
            TriggerDialogue();
        }
    }

    public virtual void ShowMeme()
    {
        if (!hasSeenMeme)
        {
            var player = FindObjectOfType<Player>();
            player.GiveTime(timeBonus);
            player.PeopleTalkedTo += 1;

        }
        hasSeenMeme = true;
        Game.UpdatePeopleList(npcName);
    }

    public void TriggerDialogue()
    {
        var speechBubblePrefab = Resources.Load<GameObject>("prefabs/SpeechBubble");
        var speechBubble = Instantiate(speechBubblePrefab).GetComponentInChildren<SpeechBubble>();
        currentSpeechBubble = speechBubble;
        speechBubble.Dialogue = hasSeenMeme ? new Queue<string>(doneDialogue) : new Queue<string>(initialDialogue);
        speechBubble.Owner = this;
        speechBubble.SetPosition(transform.position);
        var playerMovement = FindObjectOfType<PlayerMovement>();
        playerMovement.CanMove = false;
        InConversation = true;
        speechBubble.AdvanceDialogue();
    }
}
