
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnvironmentObject : MonoBehaviour
{
    private Transform playerTransform;
    private List<SpriteRenderer> spriteRenderers;

    protected virtual void Awake()
    {
        playerTransform = FindObjectOfType<Player>().transform;
        spriteRenderers = GetComponentsInChildren<SpriteRenderer>().ToList();
    }

    protected virtual void Update()
    {
        AutoSort();
    }

    private void AutoSort()
    {
        foreach (var spriteRenderer in spriteRenderers)
        {
            spriteRenderer.sortingLayerName = playerTransform.position.y > transform.position.y ?
                "ObjectsFront" : "ObjectsBehind";
        }
    }
}
