
using UnityEngine;

public class PosterPickup : MonoBehaviour
{
    [SerializeField] private int posterCount;
    private bool activated;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<Player>();
        if (!player || activated)
        {
            return;
        }

        activated = true;
        player.GivePosters(posterCount);
        TriggerGetAnimation();
        var delay = GetComponent<TweenTransforms>().duration;
        Destroy(gameObject, delay);
    }

    private void TriggerGetAnimation()
    {
        GetComponent<TweenTransforms>().Begin();
        var prefab = Resources.Load<GameObject>("prefabs/FadingText");
        var fadingText = Instantiate(prefab);
        fadingText.transform.position = transform.position + 0.5f*Vector3.up;
    }
}
